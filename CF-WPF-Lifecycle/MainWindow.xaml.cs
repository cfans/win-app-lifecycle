﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CF_WPF_Lifecycle
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            Console.WriteLine("MainWindow start");

            InitializeComponent();
            Console.WriteLine("MainWindow construtor method end");
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            Console.WriteLine("MainWindow OnInitialized");
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            Console.WriteLine("MainWindow OnSourceInitialized ");
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            Console.WriteLine("MainWindow OnActivated");
        }

        protected override void OnContentRendered(EventArgs e)
        {
            base.OnContentRendered(e);
            Console.WriteLine("MainWindow OnContentRendered");
        }

        protected override void OnDeactivated(EventArgs e)
        {
            base.OnDeactivated(e);
            Console.WriteLine("MainWindow OnDeactivated");
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            Console.WriteLine("MainWindow OnClosing");
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Console.WriteLine("MainWindow OnClosed");
        }


        private void button_Click(object sender, RoutedEventArgs e)
        {
            WindowSecond window = new WindowSecond();
            window.Show();
        }


        /*
        public override void BeginInit()
        {
            base.BeginInit();
            Console.WriteLine("MainWindow BeginInit");

        }
        public override void EndInit()
        {
            base.EndInit();
            Console.WriteLine("MainWindow EndInit");

        }
*/
    }
}
