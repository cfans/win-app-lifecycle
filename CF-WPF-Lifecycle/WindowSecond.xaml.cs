﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CF_WPF_Lifecycle
{
    /// <summary>
    /// WindowSecond.xaml 的交互逻辑
    /// </summary>
    public partial class WindowSecond : Window
    {
        public WindowSecond()
        {
            Console.WriteLine("WindowSecond constructor");
            InitializeComponent();
            Console.WriteLine("WindowSecond constructor end");

        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            Console.WriteLine("WindowSecond OnInitialized");

        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            Console.WriteLine("WindowSecond OnSourceInitialized ");
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            Console.WriteLine("WindowSecond OnActivated");

        }

        protected override void OnContentRendered(EventArgs e)
        {
            base.OnContentRendered(e);
            Console.WriteLine("WindowSecond OnContentRendered");
        }

        protected override void OnDeactivated(EventArgs e)
        {
            base.OnDeactivated(e);
            Console.WriteLine("WindowSecond OnDeactivated");
        }


        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            Console.WriteLine("WindowSecond OnClosing");

        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Console.WriteLine("WindowSecond OnClosed");
        }


        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            
        }
    }
}
