﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CF_WPF_Lifecycle
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        Mutex mutex = null;
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            bool isRunning = false;
            mutex = new Mutex(true, "SingleInstance", out isRunning);

            if (!isRunning)
            {
                MessageBox.Show("程序正在运行中，无法启动另一个实例！", "系统提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                this.Shutdown();
            }
            Console.WriteLine("App OnStartup");
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            Console.WriteLine("App OnActivated");

        }

        protected override void OnDeactivated(EventArgs e)
        {
            base.OnDeactivated(e);
            Console.WriteLine("App OnDeactivated");

        }
  
        protected override void OnSessionEnding(SessionEndingCancelEventArgs e)
        {
            base.OnSessionEnding(e);
            Console.WriteLine("App OnSessionEnding");

        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            Console.WriteLine("App OnExit");

        }
    }
}
